package com.piter.newsreader

import android.app.Application
import com.piter.newsreader.log.DebugTree
import timber.log.Timber

class NewsReaderApp : Application() {

    override fun onCreate() {
        super.onCreate()
        Timber.plant(DebugTree())
    }
}
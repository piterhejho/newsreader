package com.piter.newsreader.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.piter.newsreader.data.Article
import com.piter.newsreader.data.ResponseModel
import com.piter.newsreader.model.NewsApiServiceImpl
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableSingleObserver
import io.reactivex.schedulers.Schedulers
import timber.log.Timber

class NewsListViewModel : ViewModel() {

    private val newsList = MutableLiveData<List<Article>>()
    private val loadDataError = MutableLiveData<Boolean>()
    private val loadInProgress = MutableLiveData<Boolean>()

    private val disposable = CompositeDisposable()
    private val newsApiService = NewsApiServiceImpl()

    fun getNewsList(): LiveData<List<Article>> = newsList
    fun getLoadDataError(): LiveData<Boolean> = loadDataError
    fun getLoadInProgress(): LiveData<Boolean> = loadInProgress

    fun getData() {
        getServerData()
    }

    private fun getServerData() {
        loadInProgress.postValue(true)
        loadDataError.postValue(false)
        disposable.add(
            newsApiService.getNews()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(object :
                    DisposableSingleObserver<Pair<ResponseModel, ResponseModel>>() {
                    override fun onSuccess(pair: Pair<ResponseModel, ResponseModel>) {
                        Timber.d("success")
                        val list = ArrayList<Article>()
                        pair.first.articles?.let { list.addAll(pair.first.articles!!) }
                        pair.second.articles?.let { list.addAll(pair.second.articles!!) }
                        loadDataError.postValue(false)
                        loadInProgress.postValue(false)
                        newsList.postValue(list)
                    }

                    override fun onError(e: Throwable) {
                        Timber.e("error: %s", e.toString())
                        loadDataError.postValue(true)
                        loadInProgress.postValue(false)
                    }

                })
        )
    }
}
package com.piter.newsreader.log

import timber.log.Timber

class DebugTree : Timber.DebugTree() {
    override fun log(priority: Int, tag: String?, message: String, t: Throwable?) {
        val tagWithPrefix = "NEWS_$tag"
        super.log(priority, tagWithPrefix, message, t)
    }
}
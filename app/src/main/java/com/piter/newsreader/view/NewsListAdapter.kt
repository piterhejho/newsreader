package com.piter.newsreader.view

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.FragmentManager
import androidx.recyclerview.widget.RecyclerView
import com.piter.newsreader.R
import com.piter.newsreader.data.Article
import com.piter.newsreader.databinding.ItemNewsBinding
import timber.log.Timber

class NewsListAdapter(private val newsList: ArrayList<Article>, private val fragmentManager: FragmentManager) :
    RecyclerView.Adapter<NewsListAdapter.NewsViewHolder>(), NewsClickListener {

    private val contentDialog = ContentDialog()

    fun updateNewsList(newNewsList: List<Article>) {
        newsList.clear()
        newsList.addAll(newNewsList)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NewsViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val view =
            DataBindingUtil.inflate<ItemNewsBinding>(inflater, R.layout.item_news, parent, false)
        return NewsViewHolder(view)

    }

    override fun getItemCount() = newsList.size

    override fun onBindViewHolder(holder: NewsViewHolder, position: Int) {
        holder.view.article = newsList[position]
        holder.view.listener = this
    }

    class NewsViewHolder(var view: ItemNewsBinding) : RecyclerView.ViewHolder(view.root)

    override fun onClick(v: View) {
        for (news in newsList) {
            if (news.url == v.tag) {
                news.description?.let { contentDialog.showContent(fragmentManager, news.description) }
            }
        }

    }
}
package com.piter.newsreader.view

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import com.piter.newsreader.R
import com.piter.newsreader.databinding.FragmentNewsListBinding
import com.piter.newsreader.viewmodel.NewsListViewModel
import timber.log.Timber

class NewsListFragment : Fragment() {

    private lateinit var newsAdapter: NewsListAdapter
    private lateinit var viewModel: NewsListViewModel
    private lateinit var binding:  FragmentNewsListBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentNewsListBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        newsAdapter = NewsListAdapter(arrayListOf(), parentFragmentManager)
        viewModel = ViewModelProvider(this).get(NewsListViewModel::class.java)
        binding.getDataBt.setOnClickListener {
            viewModel.getData()
        }


        binding.newsList.apply {
            layoutManager = GridLayoutManager(context, 1)
            adapter = newsAdapter
        }

        viewModel.getNewsList().observe(viewLifecycleOwner, { list ->
            binding.loadProgress.visibility = View.GONE
            binding.loadErrorText.visibility = View.GONE
            binding.newsList.visibility = View.VISIBLE
            newsAdapter.updateNewsList(list)
        })

        viewModel.getLoadDataError().observe(viewLifecycleOwner, {
            if (it) {
                binding.newsList.visibility = View.GONE
                binding.loadProgress.visibility = View.GONE
                binding.loadErrorText.visibility = View.VISIBLE
            }
        })

        viewModel.getLoadInProgress().observe(viewLifecycleOwner, {
            if (it) {
                binding.newsList.visibility = View.GONE
                binding.loadProgress.visibility = View.VISIBLE
                binding.loadErrorText.visibility = View.GONE
            }
        })

        viewModel.getData()
    }
}
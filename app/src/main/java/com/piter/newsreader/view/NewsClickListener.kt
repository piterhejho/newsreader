package com.piter.newsreader.view

import android.view.View

interface NewsClickListener {
    fun onClick(v: View)
}
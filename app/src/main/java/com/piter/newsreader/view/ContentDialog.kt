package com.piter.newsreader.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.FragmentManager

import com.piter.newsreader.databinding.DialogContentBinding

class ContentDialog : DialogFragment() {

    private lateinit var binding: DialogContentBinding
    private var content: String? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DialogContentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onStart() {
        super.onStart()
        dialog?.window?.setLayout(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.WRAP_CONTENT
        )
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        content?.let {
            binding.contentText.text = content
        }

        binding.okBtn.setOnClickListener {
            dismiss()
        }
        dialog?.window?.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE)
        dialog?.window!!.setBackgroundDrawableResource(android.R.color.transparent)
    }

    fun showContent(manager: FragmentManager, contentToshow: String) {
        this.content = contentToshow
        show(manager, "")
    }
}
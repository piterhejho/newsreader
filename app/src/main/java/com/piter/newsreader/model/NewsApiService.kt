package com.piter.newsreader.model

import com.piter.newsreader.data.ResponseModel
import io.reactivex.Single

interface NewsApiService {
    fun getNews(): Single<Pair<ResponseModel, ResponseModel>>
}
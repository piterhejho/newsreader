package com.piter.newsreader.model

import com.piter.newsreader.data.ResponseModel
import io.reactivex.Single
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

class NewsApiServiceImpl : NewsApiService {

    private val BASE_URL = "https://newsapi.org/v2/"
    private val COUNTRY = "us"
    private val CATEGORY_BUSINESS = "business"
    private val CATEGORY_SPORT = "sport"
    private val API_KEY = "8ccc36d15790408696817cf2b16d51a4"

    private val api = Retrofit.Builder()
        .baseUrl(BASE_URL)
        .addConverterFactory(GsonConverterFactory.create())
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .build()
        .create(NewsApi::class.java)

    override fun getNews(): Single<Pair<ResponseModel, ResponseModel>> {
        val businessResponse =
            api.getLatestNews(COUNTRY, CATEGORY_BUSINESS, API_KEY)
        val sportResponse = api.getLatestNews(COUNTRY, CATEGORY_SPORT, API_KEY)
        return businessResponse.zipWith(sportResponse) { b, s -> Pair(b, s) }
    }
}
package com.piter.newsreader.model

import com.piter.newsreader.data.ResponseModel
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface NewsApi {

    @GET("top-headlines")
    fun getLatestNews(
        @Query("country") country: String,
        @Query("category") category: String,
        @Query("apiKey") apiKey: String): Single<ResponseModel>

}
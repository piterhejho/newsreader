package com.piter.newsreader.data

data class SourceModel(
    val id: String?,
    val name: String?
)

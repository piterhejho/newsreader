package com.piter.newsreader.data

data class ResponseModel (
    val status: String?,
    val totalResults: Int?,
    val articles: List<Article>?)
